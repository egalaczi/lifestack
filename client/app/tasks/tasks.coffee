'use strict'

angular.module 'lifetreeApp'
.config ($routeProvider) ->
  $routeProvider.when '/tasks',
    templateUrl: 'app/tasks/tasks.html'
    controller: 'TasksCtrl'
