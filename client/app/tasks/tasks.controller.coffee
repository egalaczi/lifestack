'use strict'

angular.module 'lifetreeApp'
.controller 'TasksCtrl', ($scope, $http, socket) ->
  $scope.tasks = []
  $scope.model = {selected: null}
  $http.get('/api/tasks').success (awesomeThings) ->
    $scope.tasks = awesomeThings
    socket.syncUpdates 'task', $scope.tasks

  $scope.addTask = ->
    return if $scope.newTask is ''
    $http.post '/api/tasks',
      name: $scope.newTask

    $scope.newTask = ''

  $scope.deleteTask = (thing) ->
    $http.delete '/api/tasks/' + thing._id

  $scope.saveTask = () ->
    task = $scope.model.selectedTask
    $http.put '/api/tasks/' + task._id ,
      task

  $scope.$on '$destroy', ->
    socket.unsyncUpdates 'tasks'
  $scope.onDragComplete = (data,evt) ->
       console.log("drag success, data:", data)
