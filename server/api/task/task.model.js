'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var TaskSchema = new Schema({
  name: String,
  info: String,
  active: Boolean,
  state: String,
  description: String
});

module.exports = mongoose.model('Task', TaskSchema);